#include <stdio.h>
/*
 * La función par_impar recibe un entero y 
 * determina si el número es par o impar
 */
void Par_impar()
{
	int valor_inicial = 0;
	
	printf("Introduzca el valor del número: ");
	scanf("%d", &valor_inicial);
	
	if (valor_inicial%2 == 0) {
		printf("%d es par \n", valor_inicial);
	} else {
		printf("%d es impar \n", valor_inicial);
	}	
		return;
}


/* La función Fibonacci recibe un entero n, la secuencia 
 * seguirá hasta n términos. 0 y 1 son los casos base (term1, term2 así que
 * simplemente se imprimen, el siguiente término es la suma de estos.
 * El mismo proceso se sigue con los siguientes.
 */
 
void  Fibonacci()
{
	int i = 3, n = 0, term_1 = 0, term_2 = 1, next_term = 0;
	
	printf("Introduzca el número de término para la sucesión de Fibonacci: ");
	scanf("%d", &n);
	
	printf("La sucesión es: 0, 1, ");
	
	while (i <= n) {
		next_term = term_1 + term_2;
		printf("%d, ", next_term);
		term_1 = term_2;
		term_2 = next_term;
		i++;
	}

	printf("\n");

	return;
}

/*
 *La función recibe un entero al cual se le calculará el factorial.
 * Mientras el n sea mayor que el contador se procede a multiplicar 
 * el contador con el facto. 
 */
 	
void Factorial()
{
	int Facto =1, n=0, count =0; 
	
	printf("Introduzca un número para calcular el factorial: ");
	scanf("%d", &n);
	
	for (count = 1;  count <= n; count++){
		Facto = Facto*count;
	}
	
	printf("El factorial del número %d es: %d \n", n, Facto);
	
	return;
}

/*
 * La función recibe un int al cual se le calcula su largo.
 * Para esto se va dividiendo el número  en 10 para
 * reducir sus digitos y por cada vez que se pueda hacer
 * se suma uno al contador
 */
 
void Largo_Numero_For()
{
	int n=0, count=0;
	
	printf("Introduzca un número para calcular su largo: ");
	scanf("%d", &n);
	
	for (count = 0; n> 0; n = n/10){
		count++;
	}
	
	printf("El largo del número es: %d \n",  count);
	
	return;
}

/* La función recibe un entero al cual se le calcula 
 * su largo
 */
 
void Largo_Numero_While()
{
	int n=0, count=1;
	
	printf("Introduzca un número para calcular su largo: ");
	scanf("%d", &n);
	
	while (n/10 > 0){
		n = n/10;
		count++;
	}
	
	printf("El largo del número  es: %d \n",  count);
	
	return;
}

/* Se calcula la sumatoria de 0 a n con un for loop
 */

void Sumatoria_For()
{
	int n=0, count = 0, sumatoria= 0;
	
	printf("Introduzca un número para calcular la sumatoria: ");
	scanf("%d", &n);
	
	for (int count = 1; count <= n; count++){
		sumatoria = sumatoria+count;
	}
	
	printf("La sumatoria es: %d \n", sumatoria);
	
	return;
}
	
/* Se calcula la sumatoria de 0 a n usando un while loop
 */
 	
void Sumatoria_While()
{
	int n=0, count=1, sumatoria=0;
	
		printf("Introduzca un número para calcular la sumatoria: ");
		scanf("%d", &n);
		
		while (count <=n){
			sumatoria= sumatoria+count;
			count++;
		}
		
		printf("La sumatoria es: %d \n", sumatoria);
		
		return;
}

/*
 *La función se encarga de invertir el número dado.
 * Se saca el módulo de n para conseguir el último digito.
 * Se multiplica por 10 para ir aumentandolo al mismo tiempo que
 * el n va disminuyendo
 */

void Invertir_numero()
{
	int n= 0, invertido=0, ultimo=0;
	
	printf("Ingrese un número para invertir: ");
	scanf("%d", &n);
	
	while (n != 0){
		ultimo = n%10;
		invertido = ultimo + invertido*10;
		n = n/10;
	}
	
	printf("El número invertido es: %d \n", invertido);
	
	return;
}

/*
 * La función recibe un entero el cual se invierte.
 * Si es igual invertido a sin invertir entonces se cosidera
 * palindromo
 */
	
void Palindromo()
{
	int n= 0, invertido=0, ultimo=0, sin_invertir=0;
	
	printf("Ingrese un número: ");
	scanf("%d", &n);
	
	sin_invertir= n;
	
	while (n != 0){
		ultimo = n%10;
		invertido = ultimo + invertido*10;
		n = n/10;
	}
		
	if (sin_invertir == invertido) {
		printf("Es palindromo. \n");
	}else {
		printf("No es palindromo. \n");
	}
	
	return;
}	
	
	

int main(void)
{
	//Se hace la llamada a la función par_impar
	//Par_impar();
	
	//Se hace la llamada a la función Fibonacci
	//Fibonacci();
	
	//Se hace la llamada a la función Factorial
	//Factorial();
	
	//Se hace la llamada a la función Largo_Numero_For
	//Largo_Numero_For();
	
	// Se hace la llamada a la función Largo_Numero_While
	//Largo_Numero_While();
	
	//Se llama a la función Sumatoria_For
	//Sumatoria_For();
	
	// Se llama a la función sumatoria_While
	//Sumatoria_While();
	
	// Se llama a la función invertir_número
	Invertir_numero();
	
	//Se llama a la función Palindromo
	//Palindromo();
	
	
	return 0;	
}
