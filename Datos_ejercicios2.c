#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*Ejercicios 2
 * Nombre : Nicole López Sosa
 * Carnet: 2020239571
 */

/*La función recibe un string y un caracter que se decide remover.
 * Para esto se crean dos punteros uno el cual es la fuente y el otro que será el destino.
 * Si el puntero fuente es diferente al caracter entonces se aumenta pdestino. Si no entonces se aumenta
 * fuente. De esta manera devuelve el string sin el caracter.
 */

void Eliminar_caracter(char * pfuente, char caracter)
{
	char * pdestino = pfuente;
	
	if (*pfuente == '\0'){
		return;
	}
	
	while (*pfuente != '\0')
	{
		if (*pfuente != caracter) 
			*pdestino++ = *pfuente;
		pfuente++;
	}
	*pdestino= '\0';
}


/* Esta funcion recibe un string, para después crear dos punteros.
 * El de texto y final. Ya con esto se aproximan por izquierda y derecha intercambiando caracteres.
 */

void Invertir_texto(char* texto)
{
	char * final = texto + strlen(texto)-1;
	
	if (*texto == '\0'){
		return;
	}
	
	while (final > texto) {
		char temporal = *texto;
		*texto = *final;
		*final = temporal;
		texto++;
		final--;
	}
	
	return;
}




/* La función recibe un texto literal. Se usa calloc para crear ese nuevo
 * espacio en el heap y después se copia a este.
 */


void copiar_heap( char* texto)
{
	int largo = 0;
	
	largo = strlen(texto);
	
	char* texto_heap = calloc(largo, sizeof(char));
	
	strcpy(texto_heap, texto);
	
	printf("El texto_heap es: %s \n", texto_heap);
	
	return;
}
	


int main()
{
	char str[] = "cccca";

	
	// Aquí se llama a la función eliminar_caracter.
	//Eliminar_caracter(str, 'c');
	//printf("Su texto sin el caracter 'c' es: %s \n", str);
	
	
	Invertir_texto(str);
	printf("Inverso es : %s \n", str);
	
	//Aquí se llama a la función copiar_heap(str)
	//copiar_heap(str);
	
	return 0;
}
		
	
	

		
	
	
	
	
	
